package com.adel.project;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ConverterActivity extends AppCompatActivity {

    private EditText km1ET;
    private EditText m2ET;
    private EditText cm3ET;
    private EditText mm4ET;
    private EditText micM5ET;
    private EditText nm6ET;
    private EditText mile7ET;
    private EditText yard8ET;
    private EditText foot9ET;
    private EditText inch10ET;

    private Button convert1;
    private Button convert2;
    private Button convert3;
    private Button convert4;
    private Button convert5;
    private Button convert6;
    private Button convert7;
    private Button convert8;
    private Button convert9;
    private Button convert10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);

        km1ET = findViewById(R.id.km1);
        m2ET = findViewById(R.id.m2);
        cm3ET = findViewById(R.id.cm3);
        mm4ET = findViewById(R.id.mm4);
        micM5ET = findViewById(R.id.micm5);
        nm6ET = findViewById(R.id.nm6);
        mile7ET = findViewById(R.id.mile7);
        yard8ET = findViewById(R.id.yard8);
        foot9ET = findViewById(R.id.foot9);
        inch10ET = findViewById(R.id.inch10);

        convert1 = findViewById(R.id.convert1);
        convert2 = findViewById(R.id.convert2);
        convert3 = findViewById(R.id.convert3);
        convert4 = findViewById(R.id.convert4);
        convert5 = findViewById(R.id.convert5);
        convert6 = findViewById(R.id.convert6);
        convert7 = findViewById(R.id.convert7);
        convert8 = findViewById(R.id.convert8);
        convert9 = findViewById(R.id.convert9);
        convert10 = findViewById(R.id.convert10);


        convert1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!km1ET.getText().toString().isEmpty()) {
                    double val = Double.parseDouble(km1ET.getText().toString());
                    m2ET.setText(String.valueOf(val * 1000));
                    cm3ET.setText(String.valueOf(val * 100000));
                    mm4ET.setText(String.valueOf(val * (Math.E + 6)));
                    micM5ET.setText(String.valueOf(val * (Math.E + 9)));
                    nm6ET.setText(String.valueOf(val * (Math.E + 12)));
                    mile7ET.setText(String.valueOf(val * 1.609));
                    yard8ET.setText(String.valueOf(val * 1093.613));
                    foot9ET.setText(String.valueOf(val * 3280.84));
                    inch10ET.setText(String.valueOf(val * 39370.079));
                }
            }
        });

        convert2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!m2ET.getText().toString().isEmpty()) {
                    double val = Double.parseDouble(m2ET.getText().toString());
                    km1ET.setText(String.valueOf(val / 1000));
                    cm3ET.setText(String.valueOf(val * 100));
                    mm4ET.setText(String.valueOf(val * 1000));
                    micM5ET.setText(String.valueOf(val * (Math.E + 6)));
                    nm6ET.setText(String.valueOf(val * (Math.E + 9)));
                    mile7ET.setText(String.valueOf(val / 1609.344));
                    yard8ET.setText(String.valueOf(val * 1.094));
                    foot9ET.setText(String.valueOf(val * 3.281));
                    inch10ET.setText(String.valueOf(val * 39.37));
                }
            }
        });

        convert3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cm3ET.getText().toString().isEmpty()) {
                    double val = Double.parseDouble(cm3ET.getText().toString());
                    km1ET.setText(String.valueOf(val / 100000));
                    m2ET.setText(String.valueOf(val / 100));
                    mm4ET.setText(String.valueOf(val * 10));
                    micM5ET.setText(String.valueOf(val * 10000));
                    nm6ET.setText(String.valueOf(val * (Math.E + 7)));
                    mile7ET.setText(String.valueOf(val / 160934.4));
                    yard8ET.setText(String.valueOf(val / 91.44));
                    foot9ET.setText(String.valueOf(val / 30.48));
                    inch10ET.setText(String.valueOf(val / 2.54));
                }
            }
        });
        convert4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mm4ET.getText().toString().isEmpty()) {
                    double val = Double.parseDouble(mm4ET.getText().toString());
                    km1ET.setText(String.valueOf(val / (Math.E + 6)));
                    m2ET.setText(String.valueOf(val / 1000));
                    cm3ET.setText(String.valueOf(val / 10));
                    micM5ET.setText(String.valueOf(val * 1000));
                    nm6ET.setText(String.valueOf(val * (Math.E + 6)));
                    mile7ET.setText(String.valueOf(val / ((1.609 * Math.E) + 6)));
                    yard8ET.setText(String.valueOf(val / 914.4));
                    foot9ET.setText(String.valueOf(val / 304.8));
                    inch10ET.setText(String.valueOf(val / 25.4));
                }
            }
        });
        convert5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!micM5ET.getText().toString().isEmpty()) {
                    double val = Double.parseDouble(micM5ET.getText().toString());
                    km1ET.setText(String.valueOf(val / (Math.E + 9)));
                    m2ET.setText(String.valueOf(val / (Math.E + 6)));
                    cm3ET.setText(String.valueOf(val / 10000));
                    mm4ET.setText(String.valueOf(val / 1000));
                    nm6ET.setText(String.valueOf(val * 1000));
                    mile7ET.setText(String.valueOf(val / ((1.609 * Math.E) + 9)));
                    yard8ET.setText(String.valueOf(val / 914400));
                    foot9ET.setText(String.valueOf(val / 304800));
                    inch10ET.setText(String.valueOf(val / 25400));
                }
            }
        });

        convert6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!nm6ET.getText().toString().isEmpty()) {
                    double val = Double.parseDouble(nm6ET.getText().toString());
                    km1ET.setText(String.valueOf(val / (Math.E + 12)));
                    m2ET.setText(String.valueOf(val / (Math.E + 9)));
                    cm3ET.setText(String.valueOf(val / (Math.E + 7)));
                    mm4ET.setText(String.valueOf(val / (Math.E + 6)));
                    micM5ET.setText(String.valueOf(val / 1000));
                    mile7ET.setText(String.valueOf(val / ((1.609 * Math.E) + 12)));
                    yard8ET.setText(String.valueOf(val / ((9.144 * Math.E) + 8)));
                    foot9ET.setText(String.valueOf(val / ((3.048 * Math.E) + 8)));
                    inch10ET.setText(String.valueOf(val / ((2.54 * Math.E) + 7)));
                }
            }
        });


        convert7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mile7ET.getText().toString().isEmpty()) {
                    double val = Double.parseDouble(mile7ET.getText().toString());
                    km1ET.setText(String.valueOf(val * 1.609));
                    m2ET.setText(String.valueOf(val * 1609.344));
                    cm3ET.setText(String.valueOf(val * 160934.4));
                    mm4ET.setText(String.valueOf(val * ((1.609 * Math.E) + 6)));
                    micM5ET.setText(String.valueOf(val * ((1.609 * Math.E) + 9)));
                    nm6ET.setText(String.valueOf(val * ((1.609 * Math.E) + 12)));
                    yard8ET.setText(String.valueOf(val * 1760));
                    foot9ET.setText(String.valueOf(val * 5280));
                    inch10ET.setText(String.valueOf(val * 63360));
                }
            }
        });


        convert8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!yard8ET.getText().toString().isEmpty()) {
                    double val = Double.parseDouble(yard8ET.getText().toString());
                    km1ET.setText(String.valueOf(val / 1093.613));
                    m2ET.setText(String.valueOf(val / 1.094));
                    cm3ET.setText(String.valueOf(val * 91.44));
                    mm4ET.setText(String.valueOf(val * 914.4));
                    micM5ET.setText(String.valueOf(val * 914400));
                    nm6ET.setText(String.valueOf(val * ((9.144 * Math.E) + 8)));
                    mile7ET.setText(String.valueOf(val / 1760));
                    foot9ET.setText(String.valueOf(val * 3));
                    inch10ET.setText(String.valueOf(val * 36));
                }
            }
        });


        convert9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!foot9ET.getText().toString().isEmpty()) {
                    double val = Double.parseDouble(foot9ET.getText().toString());
                    km1ET.setText(String.valueOf(val / 3280.84));
                    m2ET.setText(String.valueOf(val / 3.281));
                    cm3ET.setText(String.valueOf(val * 30.48));
                    mm4ET.setText(String.valueOf(val * 304.8));
                    micM5ET.setText(String.valueOf(val * 304800));
                    nm6ET.setText(String.valueOf(val * ((3.048 * Math.E) + 8)));
                    mile7ET.setText(String.valueOf(val / 5280));
                    yard8ET.setText(String.valueOf(val / 3));
                    inch10ET.setText(String.valueOf(val * 12));
                }
            }
        });


        convert10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!inch10ET.getText().toString().isEmpty()) {
                    double val = Double.parseDouble(inch10ET.getText().toString());
                    km1ET.setText(String.valueOf(val / 39370.079));
                    m2ET.setText(String.valueOf(val / 39.37));
                    cm3ET.setText(String.valueOf(val * 2.54));
                    mm4ET.setText(String.valueOf(val * 25.4));
                    micM5ET.setText(String.valueOf(val * 25400));
                    nm6ET.setText(String.valueOf(val * ((2.54 * Math.E) + 7)));
                    mile7ET.setText(String.valueOf(val / 63360));
                    yard8ET.setText(String.valueOf(val / 36));
                    foot9ET.setText(String.valueOf(val / 12));
                }
            }
        });


    }
}
