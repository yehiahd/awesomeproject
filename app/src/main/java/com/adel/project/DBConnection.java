package com.adel.project;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBConnection extends SQLiteOpenHelper {

    public static final int VERSION = 3;
    public static final String DBName = "user.db";

    public DBConnection(Context context) {

        super(context, DBName, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table IF NOT EXISTS user (name TEXT,pass TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table if EXISTS user");
        onCreate(db);
    }

    public void addUser(String userName, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", userName);
        contentValues.put("pass", password);
        db.insert("user", null, contentValues);
    }

    public boolean checkUser(String userName, String password) {
        String query = "SELECT * FROM user WHERE name=" + "\'" + userName + "\'" + " AND pass=" + "\'" + password + "\'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        return cursor.getCount() != 0;
    }
}
