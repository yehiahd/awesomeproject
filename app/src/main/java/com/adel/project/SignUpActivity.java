package com.adel.project;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {

    private EditText userNameET;
    private EditText passwordET;
    private Button signUpBtn;

    private DBConnection dbConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        dbConnection = new DBConnection(this);

        userNameET = findViewById(R.id.userNameET);
        passwordET = findViewById(R.id.passwordET);
        signUpBtn = findViewById(R.id.signUpBtn);


        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!userNameET.getText().toString().trim().isEmpty() && !passwordET.getText().toString().trim().isEmpty()) {
                    dbConnection.addUser(userNameET.getText().toString().trim(), passwordET.getText().toString());
                    Toast.makeText(SignUpActivity.this, "Okay", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SignUpActivity.this, "Enter your data!", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }
}
